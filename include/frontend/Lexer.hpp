#pragma once

#include "tokens.hpp"
#include <vector>

class Lexer {
private:
    std::vector<Token> output;
    std::vector<char> source;
    char current;
    int index;
    int line;
public:
    Lexer(std::vector<char> source);
    std::vector<Token> lex();
    void advance();
    void skipWhitespace();
    void lexKeywordOrIdentifier();
    void lexInteger();
    void optimizePunctuation();
};