#pragma once

enum TokenKind {
    // File data tokens
    TOK_EOF, // End of file
    // Types Keywords
    TOK_INT, // int
    // Operators
    TOK_ASSIGN, // ::
    // Punctuation
    TOK_LEFT_PAREN, // (
    TOK_RIGHT_PAREN, // )
    TOK_LEFT_BRACE, // {
    TOK_RIGHT_BRACE, // }
    TOK_SEMICOLON, // ;
    TOK_COLON, // :
    // Keywords
    TOK_RETURN, // ret
    // Literals
    TOK_IDENTIFIER, // [a-zA-Z_][a-zA-Z0-9_]*
    TOK_INTEGER, // [0-9]+
};

typedef struct {
    TokenKind kind;
    char* value;
    int length;
    int line;
} Token;