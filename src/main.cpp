
#include <string>

#include "../include/frontend/Lexer.hpp"

int main(int argc, char** argv) {
    std::string code = "int main :: () { ret 0; }";
    std::vector<char> source(code.begin(), code.end());
    Lexer lexer(source);
    std::vector<Token> tokens = lexer.lex();
    for (auto token : tokens) {
        // print in readble format like: Token <Type: 3> <Value: 90> <Line: 1>
        printf("Token <Type: %d> <Value: %.*s> <Line: %d>\n", token.kind, token.length + 1, token.value, token.line);
    }
    return 0;
}
