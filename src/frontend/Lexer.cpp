
#include "../../include/frontend/Lexer.hpp"

#include <cctype>
#include <cstdio>
#include <cstring>
#include <stdexcept>

Lexer::Lexer(std::vector<char> source) {
    if (source.empty()) {
        throw std::invalid_argument("source vector is empty");
    }
    this->output = std::vector<Token>();
    this->source = source;
    this->index = 0;
    this->current = source[0];
    this->line = 1;
}

void Lexer::advance() {
    if (this->index + 1 >= this->source.size()) {
        this->current = '\0';
    } else {
        this->index++;
        this->current = this->source[this->index];
    }
    if (this->current == '\n') this->line++;
}

void Lexer::skipWhitespace() {
    while (this->current == ' ' || this->current == '\t' || this->current == '\r') {
        this->advance();
    }
}

void Lexer::lexInteger() {
    int length = 0;
    while (this->current >= '0' && this->current <= '9') {
        this->advance();
        length++;
    }
    if (length == 0) {
        throw std::invalid_argument("no digits to lex");
    }
    this->output.push_back(Token {
        TOK_INTEGER,
        &this->source[this->index - length],
        length,
        this->line
    });
}

void Lexer::lexKeywordOrIdentifier() {
    int length = 0;
    while (this->current >= 'a' && this->current <= 'z' || this->current >= 'A' && this->current <= 'Z' || this->current == '_') {
        this->advance();
        length++;
    }
    if (length == 0) {
        throw std::invalid_argument("no characters to lex");
    }
    if (length == 3 && strncmp(&this->source[this->index - length], "int", length) == 0) {
        this->output.push_back(Token {
            TOK_INT,
            &this->source[this->index - length],
            length,
            this->line
        });
    } else if (length == 3 && strncmp(&this->source[this->index - length], "ret", length) == 0) {
        this->output.push_back(Token {
            TOK_RETURN,
            &this->source[this->index - length],
            length,
            this->line
        });
    } else {
        this->output.push_back(Token {
            TOK_IDENTIFIER,
            &this->source[this->index - length],
            length,
            this->line
        });
    }
}

void Lexer::optimizePunctuation() {
    // if in the `output` we have two consecutive tokens that are punctuation, we can merge them into one token
    // for example, if we have `::` we can merge them into one token `TOK_ASSIGN`
    // this is useful for the parser, because we don't have to check for two tokens, we can just check for one
    for (int i = 0; i < this->output.size() - 1; i++) {
        if (this->output[i].kind == TOK_COLON && this->output[i + 1].kind == TOK_COLON) {
            this->output[i].kind = TOK_ASSIGN;
            // : -> :: -> add a second : to the value of the token
            this->output[i].value = "::";
            this->output.erase(this->output.begin() + i + 1);
        }
    }
}

std::vector<Token> Lexer::lex() {
    if (this->source.empty()) {
        throw std::invalid_argument("source vector is empty");
    }
    while (this->current != '\0') {
        switch (this->current) {
            case ' ':
            case '\t':
            case '\r':
                this->skipWhitespace();
            break;
            case '\n':
                this->advance();
                this->line++;
            break;
            case '(':
                this->output.push_back(Token {
                    TOK_LEFT_PAREN,
                    &this->source[this->index],
                    1,
                    this->line
                });
            this->advance();

                break;
            case ')':
                this->output.push_back(Token {
                    TOK_RIGHT_PAREN,
                    &this->source[this->index],
                    1,
                    this->line
                });
            this->advance();

                break;
            case '{':
                this->output.push_back(Token {
                    TOK_LEFT_BRACE,
                    &this->source[this->index],
                    1,
                    this->line
                });
            this->advance();
break;
            case '}':
                this->output.push_back(Token {
                    TOK_RIGHT_BRACE,
                    &this->source[this->index],
                    1,
                    this->line
                });
            this->advance();

            break;
            case ';':
                this->output.push_back(Token {
                    TOK_SEMICOLON,
                    &this->source[this->index],
                    1,
                    this->line
                });
            this->advance();
            break;
            case ':':
                this->output.push_back(Token {
                    TOK_COLON,
                    &this->source[this->index],
                    1,
                    this->line
                });
            this->advance();
            break;
            default:
                // Handle integers, keywords/identifiers and error if not recognized
                    if (this->current >= '0' && this->current <= '9') {
                        this->lexInteger();
                    } else if (this->current >= 'a' && this->current <= 'z' || this->current >= 'A' && this->current <= 'Z' || this->current == '_') {
                        this->lexKeywordOrIdentifier();
                    } else {
                        printf("Unknown character: %c\n", this->current);
                        this->advance();
                    }
        }
    }
    this->output.push_back(Token {
        TOK_EOF,
        &this->source[this->index],
        0,
        this->line
    });
    this->optimizePunctuation();
    return this->output;
}
